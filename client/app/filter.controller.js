
(function() {

  "use strict";
  angular.module("PopQuizApp").controller('controllerName', controllerName);

    controllerName.$inject = ['$scope', '$filter'];
        
    function controllerName($scope, $filter) {

      var self = this; // vm or view model

      self.search ="";
      self.propertyName ="id";
      self.reverse = false;

      self.items = [
          { id: 3, name: 'GHI' },
          { id: 1, name: 'ABC' },
          { id: 5, name: 'ABCD' },
          { id: 2, name: 'DEF' },
          { id: 4, name: 'Jkl' },
      ];

      self.items2 = self.items;

      // This block can be simplified by ng-repeat="item in ctrl.items | filter: ctrl.search"

      // $scope.$watch('ctrl.search', function (val) {
      //     self.items = $filter('filter')(self.items2, val);
      // });
    
      self.sortBy = function(propertyName) {
        self.reverse = (self.propertyName === propertyName) ? !self.reverse : false;
        self.propertyName = propertyName;

      };

    };

}) ();

