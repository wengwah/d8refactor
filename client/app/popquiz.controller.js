/**
 * Client side code.
 */
(function() {
    "use strict";
    angular.module("PopQuizApp").controller("PopQuizCtrl", PopQuizCtrl);

    PopQuizCtrl.$inject = ["$http"];

    function PopQuizCtrl($http) {
        var self = this; // vm

        self.quiz = {

        };

        self.finalanswer = {
            id: 0,
            value: "",
            comments: "",
            message: ""
        };

        self.initForm = function() {
            $http.get("/popquizes")
                .then(function(result) {
                    console.log(result);
                    self.quiz = result.data;
                }).catch(function(e) {
                    console.log(e);
                });
        };

        self.initForm();

        self.submitQuiz = function() {
            console.log("submitQuiz");
            self.finalanswer.id = self.quiz.id;
            $http.post("/submit-quiz", self.finalanswer)
                .then(function(result) {
                    console.log(result);
                    if (result.data.isCorrect) {
                        self.finalanswer.message = "CORRECT !";
                    } else {
                        self.finalanswer.message = "WRONG !";
                    }
                    self.finalanswer.questionsLeft = result.data.questionsLeft;
                    self.finalanswer.totalCorrect = result.data.totalCorrect;
                }).catch(function(e) {
                    console.log(e);
                });
        };

        self.refreshScreen = function() {
            console.log("refreshing screen");
            window.location.reload(true);
            self.initForm();
        };
    }

})();